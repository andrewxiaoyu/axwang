/* Config */
const sourceDir = './src';
const outputDir = './public';
const liveServerParams = {
  port: 8000, // Set the server port. Defaults to 8080.
  host: '0.0.0.0', // Set the address to bind to. Defaults to 0.0.0.0 or process.env.IP.
  root: outputDir, // Set root directory that's being served. Defaults to cwd.
  logLevel: 2 // 0 = errors only, 1 = some, 2 = lots
};

/* Gulp */
var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var liveServer = require('live-server');

function compileSass() {
  return gulp
    .src(`${sourceDir}/sass/*.scss`)
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest(`${outputDir}/css`));
}
function compilePug() {
  return gulp
    .src(`${sourceDir}/templates/*.pug`)
    .pipe(pug().on('error', console.log))
    .pipe(gulp.dest(`${outputDir}`));
}

gulp.task('default', function() {
  gulp.start('sass');
  gulp.start('pug');
  gulp.watch(`${sourceDir}/sass/**/*.scss`, ['sass']);
  gulp.watch(`${sourceDir}/templates/**/*.pug`, ['pug']);
  liveServer.start(liveServerParams);
});

gulp.task('sass', compileSass);

gulp.task('pug', compilePug);

gulp.task('build', function() {
  gulp.start('sass');
  gulp.start('pug');
});
